# TicketSwap front-end assignment

# Explanation Midas

### time spend: approximately 8 hours

## SCSS setup
All the SCSS files I created are in `/assets/style` folder. The main.scss is the backbone and is used for including all the different files.

The folders are structured trough a variation of the [Atomic Design Methodology](http://atomicdesign.bradfrost.com/chapter-2/). I did not implement this methodology very strictly because this would add extra complexity to a small application like this. (I left out the organisms, templates and pages folders and added config and layout because I think that these folders are more generic.)

## Compiling the scss

I added a watch script which starts running continuously with the react dev server when you use `npm start`.
When you want to run the server without the watcher, you can use `npm run serve`.

The style is compiled to `src/dist/main.css`.
The `dist` is in the `src` folder because react would not import files from index.js which are outside of the src directory.
I could work around this by putting the css in the index.html, but that would mean that the style would not be hot-reloaded. I also think that the index.js is just a plain better place to import the css.

## Assets in the src
I moved the icons to the src directory.
I did this because of the same reason why I put the css in the src. React wont import files outside of the src directory.

## Classes

Here you can find a short description about all classes.
There are more detailed comments in these classes explaining my thoughts and how things work.

### SearchInstance
The SearchInstance is a class which is responsible for searching.
Search parameters (query, entities) can be set from within different classes and callbacks can be added to this class.

### Filter
This class bundles the components which handle the filtering/searching of items.
This class also is responsible for showing and hiding the category tabs.

### SearchBar
As you might expect, this class is responsible for the search bar. It handles the visuals and passes the search value to the SearchInstance class.

### FilterTabs
These are the categories which you can select under the filter bar. This class is responsible for displaying them and yet again, passing the results to the SearchInstance class.

### Overview
This class is responsible for showing the data returned by the SearchInstance class.

### OverviewCard
The only thing this class does, is showing the individual search results items.

## There was a problem...
I did not manage to get the displayType from the API...
The problem I was having is (I think) that the displayType value was not in the Searchable interface. It seemed however like it was present in the SearchableItem (`type SearchableItem implements Node & Searchable`).
I was not able to find a lot of documentation so [this file](https://raw.githubusercontent.com/artsy/metaphysics/master/_schema.graphql) is what I used to get to this conclusion.

I worked around this issue by using static data instead of the actual data.

# Assignment description

Thank you for doing this assignment! The goal is to build a simple search app using [Artsy](https://www.artsy.net/)’s public GraphQL API. We’ve already set up a few things to get you started, so you can focus on the front-end only. If you have any questions you can always [contact us](mailto:rob@ticketswap.com).

## Design and assets

Find the design as Sketch document in `/assets/sketch` as well as PNG images under `/assets/png`. We’ve also included SVG files for icons you might need in `/assets/icons`.

## Setup

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). It requires **Node** and **npm** to run. Install both via [nodejs.org](https://nodejs.org/en/) if you haven’t yet.

### Clone or download the repository

Clone or download this repository to your local machine and follow below instructions to get everything up and running.

## Available Scripts

In the project directory, you can run:

### `npm install`

Installs the required dependencies for the project.

### `npm start`

Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

All the source files are located inside `/src`. Any changes to those files will automatically reload the browser. If you want to edit the HTML template you can find it in the `/public` folder.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run build`

Generates a production-ready build.

## GraphQL API

You’ll be using Artsy’s public GraphQL API for this assignment.

We set up Apollo Client so you can start fetching data with it. Under https://metaphysics-production.artsy.net you’re able to explore the API and its documentation.

## Handing in the assignment

Once you’re done building the app as per design, you can send us a link in reply to our email.

## References

- [React docs](https://reactjs.org/)
- [Artsy GraphQL API](https://metaphysics-production.artsy.ne)

## Good luck!

And again if you have any questions, please [let us know](mailto:rob@ticketswap.com)!
