import ApolloClient, {gql} from "apollo-boost";

/**
 * This class handles all the searching functionality.
 *
 * I decided to create this class because I noticed that the data from the api was used on multiple occasions.
 * (The loading icon, the hiding of the categories, displaying results itself)
 *
 * I only wanted to query one time but I wanted to use the data on all these places. This moved me to creating a global
 * class which handles the search and notifies every thing trough callbacks.
 */
export default class SearchInstance {

  // Create a client for every instance.
  client = new ApolloClient({ uri: "https://metaphysics-production.artsy.net" });

  // Arrays for storing the callbacks.
  loadCallBacks = [];
  errorCallBacks = [];
  successCallBacks = [];

  // Default values for the query.
  query = '';
  first = 10;
  entities = [];

  /**
   * Add a function which gets called when loading results
   *
   * @param callback
   *   The function which will get called.
   */
  addLoadCallBack(callback) {
    // Check if the callback is a function and dont add it if it is not.
    if(typeof callback !== "function") return;

    this.loadCallBacks.push(callback);
  }

  /**
   * Add a function which gets called when the search query finished.
   *
   * @param callback
   *   The function which will get called.
   */
  addSuccessCallBack(callback) {
    // Check if the callback is a function and dont add it if it is not.
    if(typeof callback !== "function") return;

    this.successCallBacks.push(callback);
  }

  /**
   * Add a function which gets called when an error occurs.
   *
   * @param callback
   *   The function which will get called.
   */
  addErrorCallBack(callback) {
    // Check if the callback is a function and dont add it if it is not.
    if(typeof callback !== "function") return;

    this.errorCallBacks.push(callback);
  }

  /**
   * Set a query and search for this query.
   *
   * @param query
   *   The query.
   */
  setQuery(query) {
    this.query = query;
    this.search()
  }

  /**
   * Set the entities and search for them.
   *
   * @param entities
   *   The entities.
   */
  setEntities(entities) {
    this.entities = entities;
    this.search();
  }

  getEntities() {
    return this.entities;
  }

  /**
   * Search using the variables present in this class.
   */
  search() {

    // Construct the query.
    const query = gql`
      query getSearchResults {
        search(query: "${this.query}", first: ${this.first}, entities: [${this.entities.toString()}]) {
          edges {
            node {
              displayLabel,
              imageUrl,
              href,
            }
          }
        }
      }
    `;

    // The query is getting started. This means that we start loading.
    // Execute all added load call backs.
    this.loadCallBacks.forEach(f => f());

    // Execute the query.
    this.client.query({query})
    .then(result => {
      // 8 is the network status for errors.
      // Execute the error callbacks when this occurs.
      if(result.networkStatus === 8){
        this.errorCallBacks.forEach(f => f(result));
      } else {
      // When there are no errors, execute the success callbacks with the query result as a parameter.
        this.successCallBacks.forEach(f => f(result));
      }
    });
  }
}
