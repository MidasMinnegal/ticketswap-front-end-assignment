import React, {Component} from 'react';
import { ReactComponent as PlaceHolderImage } from '../../assets/icons/photo.svg'

export default class OverviewCard extends Component {
  render() {
    return (
      <a href={"https://artsy.net" + this.props.item.href} rel="nofollow" className='overview--card'>
        <div className="overview--card__content">
          <span className='overview--card__content--title'>{this.props.item.displayLabel}</span>
          <span className='overview--card__content--cat'>Article</span>
        </div>
        {this.props.item.imageUrl !== "" ? <img className='overview--card--image' alt={this.props.item.displayLabel} src={this.props.item.imageUrl}/> : <div className='overview--card--image'><PlaceHolderImage/></div>}
      </a>
    );
  }
}