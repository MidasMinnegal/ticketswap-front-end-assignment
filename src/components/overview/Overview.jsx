import React, {Component} from 'react';
import OverviewCard from "./OverviewCard";

export default class Overview extends Component {

  state = {
    items: [],
  };

  constructor(props) {
    super(props);

    this.showItems = this.showItems.bind(this);

    // Add a callback to this function for when the searchInstance has results
    props.searchInstance.addSuccessCallBack(this.showItems);
  }

  /**
   * Show the results
   * This function gets executed when the searchInstance finished searching without errors.
   *
   * @param result
   *   The result of the search query.
   */
  showItems(result) {
    this.setState({items: result.data.search.edges});
  }

  render() {
    return (
      <div className='overview'>
        {this.state.items.map((item, index) => <OverviewCard key = {index} item={item.node}/>)}
      </div>
    );
  }
}