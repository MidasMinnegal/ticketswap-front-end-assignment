import React, {Component} from 'react';
import { ReactComponent as SearchIcon } from '../../assets/icons/magnifying-glass.svg'
import { ReactComponent as LoadIcon } from '../../assets/icons/loading-indicator.svg'

export default class SearchBar extends Component {

  state = {
    searchValue: "",
    loading: false,
  };

  constructor(props) {
    super(props);

    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.done = this.done.bind(this);
    this.loading = this.loading.bind(this);

    // Add a callback to this function for when the searchInstance starts loading
    props.searchInstance.addLoadCallBack(this.loading);
    // Add a callback to this function for when the searchInstance has results
    props.searchInstance.addSuccessCallBack(this.done);
    // Add a callback to this function for when the searchInstance finished with an error.
    // Use the done method for this because it only sets the loading state to false.
    props.searchInstance.addErrorCallBack(this.done);
  }

  loading() {
    // The query starts, show the loading icon.
    this.setState({loading: true});
  }

  done() {
    // Loading is finished when the query is done.
    this.setState({loading: false});
  }

  /**
   * Handle the search input changes.
   *
   * @param event
   *   The change event for the search field.
   */
  handleSearchChange(event) {
    const value = event.target.value;
    this.setState({searchValue: value});

    // Set the search instance to query for the search value.
    // This also triggers the search function in the search instance.
    this.props.searchInstance.setQuery(value);
  }

  render() {
    return (
      <div className="input--wrapper search">
        {/*This input is required in order to detect of there is a value present using css.*/}
        <input type="search" value={this.state.searchValue} placeholder="Search by artist, gallery, style, theme, tag, etc." onChange={this.handleSearchChange} required/>
        <SearchIcon/>
        {this.state.loading ? <LoadIcon className="input--wrapper--load" /> : null}
      </div>
    );
  }
}