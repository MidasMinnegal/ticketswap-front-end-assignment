import React, {Component} from 'react';
import FilterTabs from "./FilterTabs";
import SearchBar from "./SearchBar";

export default class Filter extends Component {

  state = {
    tabs: false,
  };

  constructor(props) {
    super(props);

    this.showTabs = this.showTabs.bind(this);

    // Add a callback to this function for when the searchInstance has results
    this.props.searchInstance.addSuccessCallBack(this.showTabs);
  }

  /**
   * Shows or hides the tabs.
   * This method is executed when the search instance returns new results.
   *
   * @param result
   *   The result of the search query.
   */
  showTabs(result) {
    // The tabs should show when there are results or when there are any tabs selected.
    // I chose to show this when there are any tabs selected in order to help the user keep his search
    // preferences when the searchBar is empty.
    const showTabs = result.data.search.edges.length > 0 || this.props.searchInstance.getEntities().length > 0;
    this.setState({tabs: showTabs});
  }

  render() {
    return (
      <div>
        <SearchBar showTabs={this.showTabs} searchInstance={this.props.searchInstance}/>
        {this.state.tabs ? <FilterTabs searchInstance={this.props.searchInstance}/> : null}
      </div>
    );
  }
}