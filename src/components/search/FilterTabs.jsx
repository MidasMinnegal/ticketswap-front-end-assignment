import React, {Component} from 'react';
import Draggable from 'react-draggable';

export default class FilterTabs extends Component {

  // An array of selected tabs.
  selected = [];

  // Is the user dragging the component?
  dragging = false;

  // A list of all the tabs
  tabs = [
    {
      label: "Artists",
      value: "ARTIST",
    },
    {
      label: "Artworks",
      value: "ARTWORK",
    },
    {
      label: "Articles",
      value: "ARTICLE"
    },
    {
      label: "Cities",
      value: "CITY"
    },
    {
      label: "Collections",
      value: "COLLECTION"
    },
    {
      label: "Fairs",
      value: "FAIR"
    },
    {
      label: "Features",
      value: "FEATURE"
    },
    {
      label: "Galleries",
      value: "GALLERY"
    },
    {
      label: "Genes",
      value: "GENE"
    },
    {
      label: "Institutions",
      value: "INSTITUTION"
    },
    {
      label: "Profiles",
      value: "PROFILE"
    },
    {
      label: "Sales",
      value: "SALE"
    },
    {
      label: "Shows",
      value: "SHOW"
    },
    {
      label: "Tags",
      value: "TAG"
    },
  ];

  // The limit for the drag component. This value is overwritten on mount.
  state = {
    minLeft: -1200,
  };

  constructor(props) {
    super(props);

    this.tabChange = this.tabChange.bind(this);
  }

  /**
   * Change a tab and search accordingly.
   *
   * @param event
   *   The change event for the checkboxes.
   */
  tabChange(event) {
    // Do not select a tab when the user is dragging the slider.
    if(this.dragging) {
      event.preventDefault();
      return;
    }

    // Check if the checkbox is checked and add or delete values accordingly.
    // I decided to use checkboxes for this component because it allows me to visualize the selected items without
    // any custom JavaScript.
    if (event.target.checked) {
      this.selected.push(event.target.value)
    } else {
      this.selected.splice(this.selected.indexOf(event.target.value), 1)
    }

    // Set the correct entities for the searchInstance to search.
    // Setting new search values in the instance automatically triggers the search.
    this.props.searchInstance.setEntities(this.selected);
  }

  /**
   * Get the html for the tabs.
   *
   * @returns {*[]}
   *   The tabs.
   */
  getTabs() {
    // Create the tabs in a custom function in order to keep the render function clean.
    return this.tabs.map(tab => {
      return (
        <span key={tab.value}>
          <input id={tab.value} value={tab.value} onChange={this.tabChange} type="checkbox" name="overview-tabs" className="hidden" />
          <label htmlFor={tab.value}  className="overview--tabs--item">
            {tab.label}
          </label>
        </span>
      )
    })
  }

  componentDidMount() {
    // Calculate the maximum offset the Draggable component is allowed to have.
    const maxOffset = this.refs.tabsWrapper.scrollWidth - this.refs.tabsWrapper.getBoundingClientRect().width + 50;
    this.setState({minLeft: -maxOffset});
  }

  render() {
    return (
      <div className='overview--tabs'>
        <Draggable
          axis="x"
          defaultPosition={{x: 0, y: 0}}
          bounds={{left: this.state.minLeft, top: 0, right: 0, bottom: 0}}
          onDrag={() => this.dragging = true}
          onStop={() => setTimeout(() => {this.dragging = false}, 400)}
          >
          <div ref="tabsWrapper">
          {this.getTabs()}
          </div>
        </Draggable>
      </div>
    );
  }
}