import React, {Component} from "react";

import SearchInstance from './utils/SearchInstance'

import Overview from "./components/overview/Overview";
import Filter from "./components/search/Filter";

export default class app extends Component {

  search = new SearchInstance();

  render () {
    return (
      <div className="container">
        <Filter searchInstance = {this.search}/>
        <Overview searchInstance = {this.search}/>
      </div>
    )
  };
}
